<?php
  $new_roles = array('client','driver','operator','manager');
  foreach($new_roles as $new_role) {
      $role = user_role_load_by_name($new_role);
      $permissions = array();
      switch ($new_role) {
          case 'client':
              $permissions = array(
                'create alxs booking',
                'view own alxs booking',
                'edit own alxs booking',
                'delete own alxs booking'
              );
              break;
          case 'driver':
              $permissions = array(
                'view assigned alxs booking',
                'edit assigned alxs booking',
                'view own alxs booking'
              );
              break;
          case 'operator':
              $permissions = array(
                'view any alxs booking',
                'edit any alxs booking',
                'delete own alxs booking'
              );
              break;
          case 'manager':
              $permissions = array(
                'create alxs booking',
                'view assigned alxs booking',
                'edit assigned alxs booking',
                'view any alxs booking',
                'edit any alxs booking',
                'delete any alxs booking',
                'view own alxs booking',
                'edit own alxs booking',
                'delete own alxs booking',
                'administer alxs booking',
              );
              break;
          default:
              $permissions = array();
      }
      user_role_grant_permissions($role->rid, $permissions);
  }
