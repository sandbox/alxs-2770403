<?php

Class BookingHelper {
  /**
   * Create an address field on all bundles of a given entity type. 
   * Argument one is the entity type, argument 2 is the field machine_name 
   * and argument 3 is optional and is for the label.
   */
  public static function create_address($entity, $field_name, $field_label = "Address") {
    $booking_info = entity_get_info($entity);

    $field = array(
      'field_name' => $field_name,
      'type' => 'addressfield',
      'cardinality' => 1,
      'entity_types' => array($entity),
      'translatable' => FALSE,
      'weight' => 5,
    );
    $field = field_create_field($field);

    foreach ($booking_info['bundles'] as $booking_name => $booking_data) {
      $instance = array(
        'field_name' => $field_name,
        'entity_type' => $entity,
        'bundle' => $booking_name,
        'label' => $field_label,
        'required' => TRUE,
        'weight' => 10,
        'widget' => array(
          'type' => 'addressfield_standard',
          'weight' => 10,
          'settings' => array(
            'format_handlers' => array('address', 'address-hide-country'),
          ),
        ),
        'display' => array(),
      );
      field_create_instance($instance);
    }
  }

  /**
   * Assign permissions and roles as defined in $perms multi-dimensional array.
   */
  public static function assign_perms($perms) {
    foreach($perms as $roleobject => $perm) {
      $role = user_role_load_by_name($roleobject);
      user_role_grant_permissions($role->rid, $perm);
    }
    $user_roles = user_roles();
    foreach($user_roles as $user_role) {
      $role = new stdClass;
      $role = user_role_load_by_name($user_role);
      user_role_grant_permissions($role->rid, array('create alxs booking'));
    }
  }

  /**
   * Create roles defined in $role_names array as $name => $weight
   */
  public static function create_roles($role_names) {
    $roles = user_roles(TRUE);
    foreach($role_names as $new_role => $role_weight) {
      if(!in_array($new_role, $roles)) {
        $role = new stdClass();
        $role->name = $new_role;
        $role->weight = $role_weight;
        user_role_save($role);
      }
    }
  }

  /**
   * Remove roles specified in $role_names single dimension array
   */
  public static function remove_roles($role_names) {
    $roles = user_roles(TRUE);
    foreach($role_names as $role_name) {
      if(in_array($role_name, $roles)) {
        user_role_delete($role_name);
      }
    }
  }
}
