<?php

/**
 * @file
 * Describe entity and controller custom classes.
 */

/**
 * Typical Entity 4 class.
 */
class AlxsBooking extends Entity {

}

/**
 * Typical Entity 4 controller class.
 */
class AlxsBookingController extends EntityAPIController {

}
